#include <iostream>
#include "tclap/CmdLine.h"
#include "FileWriter.h"
#include "Grid.h"
#include "LinearFromCorner.h"
#include "Version.h"

int main(int argc, char **argv)
{
  // Declare parameters from CLI config
  std::size_t numberIterations{1};
  std::size_t sizeXGrid{0};
  std::size_t sizeYGrid{0};
  float       maxTemp{0.0};

  // Parse the command-line for options.
  try
  {
    TCLAP::CmdLine cmd("laplace-solver", ' ',
                       LaplaceSolver::Version::version());

    // Option to set the number of iterations.
    TCLAP::ValueArg<std::size_t> nitArg("n", "num-iter",
                                        "Number of iterations. Default is 10.",
                                        false, 10, "std::size");
    // Option to set the x size of the 2D grid
    TCLAP::ValueArg<std::size_t> nXGridArg("x", "x-grid-size",
                                           "Set the size of the x grid. Default is 1000.",
                                           false, 1000, "std::size_t");

    // Option to set the x size of the 2D grid
    TCLAP::ValueArg<std::size_t> nYGridArg("y", "y-grid-size",
                                           "Set the size of the y grid. Default is 1000.",
                                           false, 1000, "std::size_t");

    // Option to set the maximum temperature
    TCLAP::ValueArg<float> maxTempArg("t", "max-temp",
                                      "Set the maximum temperature. Default is 100.0.",
                                      false, 100.0, "float");

    cmd.add(nitArg);
    cmd.add(nXGridArg);
    cmd.add(nYGridArg);
    cmd.add(maxTempArg);

    cmd.parse(argc, argv);

    numberIterations = nitArg.getValue();
    sizeXGrid = nXGridArg.getValue();
    sizeYGrid = nYGridArg.getValue();
    maxTemp = maxTempArg.getValue();
  }
  catch(TCLAP::ArgException &e)
  {
    std::cerr << "Command-line parsing failed: " << e.error();
    std::cerr << " for arg " << e.argId() << std::endl;
  }

  std::cout << "This is the Laplace Solver!" << std::endl;
  std::cout << "Run with " << numberIterations << " iterations." << std::endl;
  std::cout << "Grid is " << sizeXGrid << "x" << sizeYGrid << std::endl;

  auto traceIter = numberIterations / 10;

  LaplaceSolver::Grid tCurr{sizeXGrid, sizeYGrid};
  LaplaceSolver::LinearFromCorner lfc{maxTemp};
  lfc.setBoundaryConditions(tCurr);
  auto tPrev = tCurr;

  //////////////////////////////////////////
  // Do Computation on grid for N iterations
  //////////////////////////////////////////

  for(auto iter = std::size_t{0}; iter < numberIterations; iter++)
  {
    for(auto i = std::size_t{1}; i <= sizeYGrid; ++i)
    {
      for(auto j = std::size_t{1}; j <= sizeXGrid; ++j)
      {
        auto val = 0.25 * (tPrev.getValueAt(i+1, j) + tPrev.getValueAt(i-1, j) +
                           tPrev.getValueAt(i, j+1) + tPrev.getValueAt(i, j-1));
        tCurr.setValueAt(i, j, val);
      }
    }
    tPrev = tCurr;

    if(iter % traceIter == 0)
    {
      std::cout << "--------------- Interation " << iter;
      std::cout << " ---------------" << std::endl;
      tCurr.trace();
    }
  }

  LaplaceSolver::FileWriter fw{};
  fw.writeGrid(tCurr);

  return 0;
}
