#include "Grid.h"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <vector>

namespace LaplaceSolver
{

const std::size_t Grid::BUFFER_X = 2;
const std::size_t Grid::BUFFER_Y = 2;

Grid::Grid() : numX{10}, numY{10}
{
  this->init();
}

/**
 * This is the parametered constructor.
 * @param nX : Size of the grid in the x direction
 * @param nY : Size of the grid in the y direction
 */
Grid::Grid(std::size_t nX, std::size_t nY) : numX{nX}, numY{nY}
{
  this->init();
}

/**
 * This is the copy constructor.
 * @param g : Object to copy the information from.
 */
Grid::Grid(const Grid &g)
{
  this->numX = g.numX;
  this->numY = g.numY;
  this->create();
  std::size_t fullSize{(g.numX + this->BUFFER_X) * (g.numY + this->BUFFER_Y)};
  for (auto i = std::size_t{0}; i < fullSize; i++)
  {
    this->grid[i] = g.grid[i];
  }
}

/**
 * This is the assignment operator.
 * @param rhs : Object to get the information from.
 * @return : The copied object;
 */
const Grid& Grid::operator=(const Grid &rhs)
{
  this->numX = rhs.numX;
  this->numY = rhs.numY;
  this->create();
  std::size_t fullSize = (rhs.numX + this->BUFFER_X) * (rhs.numY + this->BUFFER_Y);
  for (auto i = std::size_t{0}; i < fullSize; i++)
  {
    this->grid[i] = rhs.grid[i];
  }
  return *this;
}

/**
 * This function creates the holder for the grid array with the appropriate
 * size.
 */
void Grid::create()
{
  this->grid = new float[this->getFullSize()];
}

/**
 * This function creates the grid array and sets all elements to zero.
 */
void Grid::init()
{
  this->create();
  for (auto j = 0; j < (this->numY + this->BUFFER_Y); j++)
  {
    for (auto i = 0; i < (this->numX + this->BUFFER_X); i++)
    {
      this->grid[this->getLinearIndex(i, j)] = 0.0;
    }
  }
}

/**
 * This is the class destructor. It is responsible for deleting the allocated
 * memory for the grid array.
 */
Grid::~Grid()
{
  delete [] this->grid;
}

/**
 * This function takes the index values and calculates the linear index into
 * the grid array. The x is the column index, y is the row index.
 * @param i : The index for the x direction
 * @param j : The index for the y direction (fastest runner)
 * @return : The calculated linear index
 */
std::size_t Grid::getLinearIndex(const std::size_t i, const std::size_t j) const
{
  auto linearIndex = j + ((this->numY + this->BUFFER_Y) * i);
  if(linearIndex > this->getFullSize() - 1)
  {
    throw std::out_of_range("Index pair out of range");
  }
  return linearIndex;
}

/**
 * This function returns the full size of the grid as (NX + BUFF_X) *
 * (NY * BUFF_Y).
 * @return : The full size of the grid
 */
const std::size_t Grid::getFullSize() const
{
  return (this->numX + this->BUFFER_X) * (numY + this->BUFFER_Y);
}

/**
 * This function returns the full size for the x direction as (NX + BUFF_X)
 * @return  : The full size of the x direction
 */
const std::size_t Grid::getXSize() const
{
  return (this->numX + this->BUFFER_X);
}

/**
 * This function returns the size of the grid in the x direction without the
 * buffer.
 * @return : The grid size without buffer in the x direction.
 */
const std::size_t Grid::getGridXSize() const
{
  return this->numX;
}

/**
 * This function returns the size of the grid in the y direction without the
 * buffer.
 * @return : The grid size without buffer in the y direction.
 */
const std::size_t Grid::getGridYSize() const
{
  return this->numY;
}

/**
 * This function returns the full size for the y direction as (NY + BUFF_Y)
 * @return  : The full size of the y direction
 */
const std::size_t Grid::getYSize() const
{
  return (this->numY + this->BUFFER_Y);
}

/**
 * This function sets a row in the grid with the given data. The default
 * behavior is to only set the grid portion (no buffer values) with values.
 * @param index : The index of the row to set.
 * @param row : The array of values for the row.
 * @param gridOnly : Flag to specify setting grid only (true) or full grid (false).
 */
void Grid::setRow(const std::size_t index, const std::vector<float> &row,
                  const bool gridOnly)
{
  auto rowStart = this->getLinearIndex(index,
                                       static_cast<std::size_t>(gridOnly));
  std::copy(std::begin(row), std::end(row), &this->grid[rowStart]);
}

/**
 * This function sets a column in the grid with the given data. The default
 * behavior is to only set the grid portion (no buffer values) with values.
 * @param index : The index of the column to set.
 * @param col : The array of values for the row.
 * @param gridOnly : Flag to specify setting grid only (true) or full grid (false).
 */
void Grid::setColumn(const std::size_t index, const std::vector<float> &col,
                     const bool gridOnly)
{
  auto r = std::size_t{gridOnly};
  for(auto v : col)
  {
    this->grid[this->getLinearIndex(r++, index)] = v;
  }
}

/**
 * This function gets a value from the grid at the given index pair.
 * @param indexX : The column index to retrieve.
 * @param indexY : The row index to retrieve.
 * @return : The value at the given index pair.
 */
const float Grid::getValueAt(const std::size_t indexX,
                             const std::size_t indexY) const
{
  return this->grid[this->getLinearIndex(indexX, indexY)];
}

/**
 * This function sets a value on the grid at the given index pair.
 * @param indexX : The column index to retrieve.
 * @param indexY : The row index to retrieve.
 * @param value : The value to set on the grid.
 */
void Grid::setValueAt(const std::size_t indexX,
                      const std::size_t indexY,
                      const float value)
{
  this->grid[this->getLinearIndex(indexX, indexY)] = value;
}

/**
 * This function prints out the last N columns on the last row in the grid
 * (not counting the buffer row). The default is the last 10 columns.
 * @param offset : Number of columns to print.
 */
void Grid::trace(const std::size_t offset)
{
  for(auto i = this->numX - offset; i <= this->numX; ++i)
  {
    std::cout << std::setw(15) << std::setprecision(8) << this->grid[this->getLinearIndex(this->numY, i)];
  }
  std::cout << std::endl;
}

/**
 * This function returns the full grid data array.
 * @return : The grid data array.
 */
const float *Grid::getGridData() const
{
  return this->grid;
}

/**
 * This function creates an ordered array up to the full x size of the grid
 * minus one.
 * @return : The created x array.
 */
const std::vector<float> Grid::makeXAxis() const
{
  std::vector<float> v{};
  for(auto i = std::size_t{0}; i < this->getXSize(); ++i)
  {
    v.push_back(i);
  }
  return v;
}

/**
 * This function creates an ordered array up to the full y size of the grid
 * minus one.
 * @return : The created y array.
 */
const std::vector<float> Grid::makeYAxis() const
{
  std::vector<float> v{};
  for(auto i = std::size_t{0}; i < this->getYSize(); ++i)
  {
    v.push_back(i);
  }
  return v;
}

} // LaplaceSolver
