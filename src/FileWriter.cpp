#include "FileWriter.h"
#include "Grid.h"

#include <H5Cpp.h>
#include <tinyxml.h>

namespace LaplaceSolver
{

/**
 * This function is the default constructor. It sets the fileName member
 * variable to output.
 */
FileWriter::FileWriter() : fileName{"output"}
{
}

/**
 * This function is the parametered constructor.
 * @param fName : The tag to use as the filename
 */
FileWriter::FileWriter(const std::string &fName) : fileName{fName}
{
}

FileWriter::~FileWriter()
{
}

/**
 * This function returns the filename.
 * @return : The currently held filename.
 */
const std::string FileWriter::getFileName() const
{
  return this->fileName;
}

/**
 * This function writes a HDF5 file from the contents and information in the
 * grid object.
 * @param grid : Object to write file from.
 */
void FileWriter::writeGrid(const Grid &grid) const
{
  std::string outputH5 = this->fileName + ".h5";
  H5::Exception::dontPrint();

  auto *file = new H5::H5File(outputH5.c_str(), H5F_ACC_TRUNC);

  hsize_t dims[2] = {grid.getXSize(), grid.getYSize()};
  auto *dataspace = new H5::DataSpace(2, dims);
  auto *dataset = new H5::DataSet(file->createDataSet(
                                    "/temperature", H5::PredType::NATIVE_FLOAT,
                                    *dataspace));
  dataset->write(grid.getGridData(), H5::PredType::NATIVE_FLOAT);
  delete dataset;
  delete dataspace;

  auto *group = new H5::Group(file->createGroup("/grid"));

  hsize_t xaxis_rank[1] = {grid.getXSize()};
  dataspace = new H5::DataSpace(1, xaxis_rank);
  dataset = new H5::DataSet(file->createDataSet(
                              "/grid/x", H5::PredType::NATIVE_FLOAT,
                              *dataspace));
  dataset->write(grid.makeXAxis().data(), H5::PredType::NATIVE_FLOAT);
  delete dataset;
  delete dataspace;

  hsize_t yaxis_rank[1] = {grid.getYSize()};
  dataspace = new H5::DataSpace(1, yaxis_rank);
  dataset = new H5::DataSet(file->createDataSet(
                              "/grid/y", H5::PredType::NATIVE_FLOAT,
                              *dataspace));
  dataset->write(grid.makeYAxis().data(), H5::PredType::NATIVE_FLOAT);
  delete dataset;
  delete dataspace;

  delete group;
  delete file;

  this->writeXDMF(grid);
}

/**
 * This function writes a XDMF format file for use with the created HDF5 file.
 * This will allow ParaView to read in the data.
 * @param grid : The object to collect information from.
 */
void FileWriter::writeXDMF(const Grid &grid) const
{
  std::string outputXML = this->fileName + ".xmf";
  TiXmlDocument doc{};
  auto decl = new TiXmlDeclaration{"1.0", "", ""};
  doc.LinkEndChild(decl);

  auto xdmf = new TiXmlElement{"Xdmf"};
  xdmf->SetAttribute("Name", "2.0");
  doc.LinkEndChild(xdmf);

  auto domain = new TiXmlElement{"Domain"};
  xdmf->LinkEndChild(domain);

  auto gridNode= new TiXmlElement{"Grid"};
  gridNode->SetAttribute("Name", "solver_grid");
  domain->LinkEndChild(gridNode);

  // A couple of common attributes
  auto precision = new TiXmlAttribute{"Precision", "8"};
  auto format = new TiXmlAttribute{"Format", "HDF"};
  std::ostringstream os;
  os << grid.getXSize() << " " << grid.getYSize();
  auto gridDims = new TiXmlAttribute{"Dimensions", os.str()};

  auto topology = new TiXmlElement{"Topology"};
  topology->SetAttribute("TopologyType", "2DRectMesh");
  topology->SetAttribute(gridDims->Name(), gridDims->ValueStr());
  gridNode->LinkEndChild(topology);

  auto geometry = new TiXmlElement{"Geometry"};
  geometry->SetAttribute("GeometryType", "VXVY");
  gridNode->LinkEndChild(geometry);

  auto xDataItem = new TiXmlElement{"DataItem"};
  xDataItem->SetAttribute("Name", "X");
  xDataItem->SetAttribute(precision->Name(), precision->ValueStr());
  xDataItem->SetAttribute("Dimensions", grid.getXSize());
  xDataItem->SetAttribute(format->Name(), format->ValueStr());
  std::string outputH5 = this->fileName + ".h5";
  xDataItem->LinkEndChild(new TiXmlText{outputH5 + ":/grid/x"});
  geometry->LinkEndChild(xDataItem);

  auto yDataItem = new TiXmlElement{"DataItem"};
  yDataItem->SetAttribute("Name", "Y");
  yDataItem->SetAttribute(precision->Name(), precision->ValueStr());
  yDataItem->SetAttribute("Dimensions", grid.getYSize());
  yDataItem->SetAttribute(format->Name(), format->ValueStr());
  yDataItem->LinkEndChild(new TiXmlText{outputH5 + ":/grid/y"});
  geometry->LinkEndChild(yDataItem);

  auto attribute = new TiXmlElement{"Attribute"};
  attribute->SetAttribute("Center", "Node");
  attribute->SetAttribute("Name", "Temperature");
  gridNode->LinkEndChild(attribute);

  auto gridDataItem = new TiXmlElement{"DataItem"};
  gridDataItem->SetAttribute(precision->Name(), precision->ValueStr());
  gridDataItem->SetAttribute(gridDims->Name(), gridDims->ValueStr());
  gridDataItem->SetAttribute(format->Name(), format->ValueStr());
  gridDataItem->LinkEndChild(new TiXmlText{outputH5 + ":/temperature"});
  attribute->LinkEndChild(gridDataItem);

  doc.SaveFile(outputXML);
}

} // LaplaceSolver
