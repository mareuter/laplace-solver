#include "LinearFromCorner.h"

#include "Grid.h"

namespace LaplaceSolver
{

LinearFromCorner::LinearFromCorner() : temperature{10.0}
{
}

/**
 * This is the parametered constructor.
 * @param temp : The maximum temperature to use.
 */
LinearFromCorner::LinearFromCorner(float temp) : temperature{temp}
{
}

/**
 * This function returns the maximum temperature for the corner point.
 * @return : The maximum value of the temperature at the corner point.
 */
float LinearFromCorner::getTemperature()
{
  return this->temperature;
}

/**
 * This function sets the temperature for a grid as a linear dependence
 * on the maximum temperature. The temperature at the lower right-hand corner
 * is the maximum temperature. The opposite ends of the corresponding row and
 * column is zero.
 * @param grid : The object to set boundary conditions on.
 */
void LinearFromCorner::setBoundaryConditions(Grid &grid)
{
  // Create row BC
  auto xSize = grid.getXSize();
  std::vector<float> temperatureRow(xSize);
  for(auto i = std::size_t{0}; i < xSize; i++)
  {
    temperatureRow[i] = (this->temperature * static_cast<float>(i)) /
                         static_cast<float>(grid.getGridXSize());
  }
  grid.setRow(grid.getXSize() - 1, temperatureRow, false);

  // Create column BC
  auto ySize = grid.getYSize();
  std::vector<float> temperatureCol(ySize);
  // The --j jumps one further than correct, so start with full Y size.
  for(auto j = ySize; --j > 0; )
  {
    temperatureCol[j] = (this->temperature * static_cast<float>(j)) /
                         static_cast<float>(grid.getGridYSize());
  }
  grid.setColumn(grid.getYSize() - 1, temperatureCol, false);
}

} // namespace LaplaceSolver
