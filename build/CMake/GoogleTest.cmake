#
# add_google_test(<target> <sources>)
#
#  Adds a Google Test based test executable, <target> built from <sources>
#  and adds the test so that CTest will run it. Both the executable and the
#  test will be named <target>.
#

function( add_google_test target )
  add_executable( ${target} ${ARGN} )
  target_link_libraries( ${target} ${GMOCK_LIBRARIES} ${HDF5_LIBRARIES}
                                   ${TinyXML_LIBRARY})
  add_test( ${target} ${target} )
endfunction()
