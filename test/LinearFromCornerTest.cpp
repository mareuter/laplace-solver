#include "LinearFromCorner.h"
#include "Grid.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace LaplaceSolver
{
namespace testing
{

class LinearFromCornerTest : public ::testing::Test
{
protected:
  LinearFromCornerTest() {}
  virtual ~LinearFromCornerTest() {}

  virtual void SetUp() {}
  virtual void TearDown() {}

  LinearFromCorner lfc;
};

TEST_F(LinearFromCornerTest, default_constructor)
{
  EXPECT_THAT(lfc.getTemperature(), ::testing::Eq(10.0));
}

TEST_F(LinearFromCornerTest, parameter_constructor)
{
  LinearFromCorner lfc2{300.0};
  EXPECT_THAT(lfc2.getTemperature(), ::testing::Eq(300.0));
}

TEST_F(LinearFromCornerTest, set_boundary_conditions)
{
  Grid grid{3, 3};
  LinearFromCorner lfc2{30.0};
  lfc2.setBoundaryConditions(grid);
  EXPECT_THAT(grid.getValueAt(4, 2), ::testing::Eq(20.0));
  EXPECT_THAT(grid.getValueAt(1, 4), ::testing::Eq(10.0));
}

} // namespace testing
} // namespace LaplaceSolver

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
