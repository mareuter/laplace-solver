#include "FileWriter.h"
#include "Grid.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace LaplaceSolver
{
namespace testing
{

class FileWriterTest : public ::testing::Test
{
protected:
  FileWriterTest() {}
  virtual ~FileWriterTest() {}

  virtual void SetUp() {}
  virtual void TearDown() {}

  FileWriter fw;
};

TEST_F(FileWriterTest, default_constructor)
{
  EXPECT_THAT(fw.getFileName(), ::testing::Eq(std::string{"output"}));
}

TEST_F(FileWriterTest, parameter_constructor)
{
  auto str = std::string{"my_output"};
  FileWriter fw2{str};
  EXPECT_THAT(fw2.getFileName(), ::testing::Eq(str));
}

TEST_F(FileWriterTest, write_files)
{
  auto str = std::string{"test_output"};
  FileWriter fw2{str};
  Grid grid{3, 3};
  EXPECT_NO_THROW(fw2.writeGrid(grid));
}

} // namespace testing
} // namespace LaplaceSolver

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
