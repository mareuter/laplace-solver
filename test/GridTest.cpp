#include "Grid.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <stdexcept>

namespace LaplaceSolver
{
namespace testing
{

class GridTest : public ::testing::Test
{
protected:
  GridTest() {}
  virtual ~GridTest() {}

  virtual void SetUp() {}
  virtual void TearDown() {}

  Grid grid;
};

TEST_F(GridTest, default_constructor)
{
  EXPECT_THAT(grid.getFullSize(), ::testing::Eq(std::size_t{144}));
  EXPECT_THAT(grid.getXSize(), ::testing::Eq(std::size_t{12}));
  EXPECT_THAT(grid.getYSize(), ::testing::Eq(std::size_t{12}));
}

TEST_F(GridTest, parameter_constructor)
{
  Grid g{20, 20};
  EXPECT_THAT(g.getFullSize(), ::testing::Eq(std::size_t{484}));
  EXPECT_THAT(g.getXSize(), ::testing::Eq(std::size_t{22}));
  EXPECT_THAT(g.getYSize(), ::testing::Eq(std::size_t{22}));
}

TEST_F(GridTest, copy_constructor)
{
  Grid g{grid};
  EXPECT_THAT(g.getFullSize(), ::testing::Eq(std::size_t{144}));
}

TEST_F(GridTest, assignment_operator)
{
  Grid g = Grid{15, 15};
  EXPECT_THAT(g.getFullSize(), ::testing::Eq(std::size_t{289}));
  EXPECT_THAT(g.getXSize(), ::testing::Eq(std::size_t{17}));
  EXPECT_THAT(g.getYSize(), ::testing::Eq(std::size_t{17}));
}

// These tests will adjust the values in the grid.

TEST_F(GridTest, set_row)
{
  std::vector<float> row(grid.getXSize());
  auto value = float{45.0};
  auto colIndex = std::size_t{3};
  auto rowIndex = std::size_t{8};

  row[colIndex] = value;
  grid.setRow(rowIndex, row, false);
  EXPECT_THAT(grid.getValueAt(rowIndex, colIndex), ::testing::Eq(value));
}

TEST_F(GridTest, set_column)
{
  std::vector<float> col(grid.getYSize());
  auto value = float{45.0};
  auto colIndex = std::size_t{7};
  auto rowIndex = std::size_t{4};

  col[rowIndex] = value;
  grid.setColumn(colIndex, col, false);
  EXPECT_THAT(grid.getValueAt(rowIndex, colIndex), ::testing::Eq(value));
}

// End value adjustment.

TEST_F(GridTest, get_value_at)
{
  EXPECT_THAT(grid.getValueAt(1, 1), ::testing::Eq(0.0));
}

TEST_F(GridTest, get_value_outside_range)
{
  EXPECT_THROW(grid.getValueAt(20, 20), std::out_of_range);
}

TEST_F(GridTest, set_value_at)
{
  EXPECT_NO_THROW(grid.setValueAt(10, 10, -3.0));
  EXPECT_THAT(grid.getValueAt(10, 10), ::testing::Eq(-3.0));
}

TEST_F(GridTest, set_value_outside_range)
{
  EXPECT_THROW(grid.setValueAt(20, 20, -5.0), std::out_of_range);
}

} // namespace testing
} // namespace LaplaceSolver

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

