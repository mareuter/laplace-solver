#ifndef LAPLACE_SOLVER_FILEWRITER_H
#define LAPLACE_SOLVER_FILEWRITER_H

#include <string>

namespace LaplaceSolver
{
class Grid;

class FileWriter
{
public:
  /// Default constructor.
  FileWriter();
  /// Parameter constructor.
  FileWriter(const std::string &fName);
  /// Destructor.
  virtual ~FileWriter();
  /// Get filename.
  const std::string getFileName() const;
  /// Write grid to a file.
  void writeGrid(const Grid &grid) const;

private:
  /// Write a XDMF file from grid.
  void writeXDMF(const Grid &grid) const;

  std::string fileName; ///< Tag for filename (minus extension)
};

} // LaplaceSolver

#endif // LAPLACE_SOLVER_FILEWRITER_H
