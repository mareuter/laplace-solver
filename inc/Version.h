#ifndef LAPLACE_SOLVER_VERSION_H
#define LAPLACE_SOLVER_VERSION_H

namespace LaplaceSolver
{
class Version
{
public:
  /// Get the program version.
  static const char *version();

private:
  /// Private constructor since this class shouldn't be instantiated.
  Version();
};

} // LaplaceSolver

#endif // LAPLACE_SOLVER_VERSION_H
