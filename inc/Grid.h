#ifndef LAPLACE_SOLVER_GRID_H
#define LAPLACE_SOLVER_GRID_H

#include <cstddef>
#include <vector>

namespace LaplaceSolver
{

class Grid
{
public:
  /// Default constructor.
  Grid();
  /// Standard constructor.
  Grid(std::size_t nX, std::size_t nY);
  /// Copy constructor.
  Grid(const Grid &g);
  /// Destructor.
  virtual ~Grid();
  /// Assignment operator.
  const Grid& operator=(const Grid &rhs);
  /// Get the full grid size.
  const std::size_t getFullSize() const;
  /// Get the grid data array.
  const float *getGridData() const;
  /// Get value at an index pair.
  const float getValueAt(const std::size_t indexX, const std::size_t indexY) const;
  /// Get x size of the grid (without buffer).
  const std::size_t getGridXSize() const;
  /// Get y size of the grid (without buffer).
  const std::size_t getGridYSize() const;
  /// Get the full x size.
  const std::size_t getXSize() const;
  /// Get the full y size.
  const std::size_t getYSize() const;
  /// Create a x axis array.
  const std::vector<float> makeXAxis() const;
  /// Create a y axis array.
  const std::vector<float> makeYAxis() const;
  /// Set a column in the grid.
  void setColumn(const std::size_t index, const std::vector<float> &col,
                 const bool gridOnly = true);
  /// Set a row in the grid.
  void setRow(const std::size_t index, const std::vector<float> &row,
              const bool gridOnly = true);
  /// Set a value on the grid.
  void setValueAt(const std::size_t indexX, const std::size_t indexY,
                  const float value);
  /// Print last N columns on last row.
  void trace(const std::size_t offset = 10);

private:
  /// Creates the grid
  void create();
  /// Initializes grid
  void init();
  /// Get linear index
  std::size_t getLinearIndex(const std::size_t i, const std::size_t j) const;

  static const std::size_t BUFFER_X; ///< Edge buffer for grid in x direction
  static const std::size_t BUFFER_Y; ///< Edge buffer for grid in y direction
  std::size_t numX; ///< Size of grid in x direction
  std::size_t numY; ///< Size of grid in y direction
  float *grid;      ///< Holder for the grid
};

} // LaplaceSolver

#endif // LAPLACE_SOLVER_GRID_H
