#ifndef LAPLACE_SOLVER_LINEARFROMCORNER_H
#define LAPLACE_SOLVER_LINEARFROMCORNER_H

namespace LaplaceSolver
{

class Grid;

class LinearFromCorner
{
public:
  /// Default constructor.
  LinearFromCorner();
  /// Standard constructor.
  LinearFromCorner(float temp);
  /// Function to get the maximum temperature.
  float getTemperature();
  /// Function to set the boundary conditions.
  void setBoundaryConditions(Grid &grid);

private:
  float temperature; ///< The maximum temperature for the corner point.
};

} // namespace LaplaceSolver

#endif // LAPLACE_SOLVER_LINEARFROMCORNER_H
